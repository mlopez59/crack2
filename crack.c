#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    char *hashed = md5(guess, strlen(guess));
    // Compare the two hashes
    if (strncmp(hashed, hash, HASH_LEN) == 0)
    {
        printf("%s %s\n", hash, guess);
        free(hashed); 
        return 1;
    }
    // Free any malloc'd memory
    free(hashed); 
    return 0;
}

// Read in the dictionary file and return the array of strings
// and store the length of the array in size.
// This function is responsible for opening the dictionary file,
// reading from it, building the data structure, and closing the
// file.
char **read_dictionary(char *filename, int *size)
{
    FILE *f;
    f = fopen(filename, "r");
    char *dictionary = malloc(*size);
    fread(dictionary, 1, *size, f);
    dictionary[*size] = '\0';
    int diclines = 0, j = 1;
    for (int i = 0; i < *size; i++)
    {
        if (dictionary[i] == '\n' ) diclines++;
    }
    char ** findic = malloc(diclines * sizeof(char *));
    findic[0] = strtok(dictionary, "\n");
    while ((findic[j] = strtok(NULL, "\n")) != NULL)
    {
        j++; 
    }
    fclose(f); 
    return findic;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    FILE *h;
    FILE *d;
    h = fopen(argv[1], "r");
    if (!h)
    {
        printf("Could not open %s as a hash file for reading\n", argv[1]);
        exit(1);
    }
    d = fopen(argv[2], "r");
    if (!d)
    {
        printf("Could not open %s as a dictionary file for reading\n", argv[2]);
        exit(1);
    }
    fclose(d);
    

    // Read the dictionary file into an array of strings.
    int dlen, hlen;
    struct stat info;
    stat (argv[2], &info);
    dlen = (info.st_size + 1);
    char **dict = read_dictionary(argv[2], &dlen);
    
    // Open the hash file for reading.
    struct stat hinfo;
    stat(argv[1], &hinfo);
    hlen = (hinfo.st_size + 1);
    char *hashd = malloc(hlen);
    fread(hashd, 1, hlen, h);
    fclose(h);
    hashd[hlen] = '\0';
    int hashlines = 0, k = 1;
    for (int i = 0; i < hlen; i++)
    {
        if (hashd[i] == '\n' ) hashlines++;
    }
    char ** hashdic = malloc(hashlines * sizeof(char *));
    hashdic[0] = strtok(hashd, "\n");
    while ((hashdic[k] = strtok(NULL, "\n")) != NULL)
    {
        k++; 
    }

    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
    int solved = 0, o = 0, p = 0;
    for (int u = 0; u < hashlines; u++)
    {
        p = 0;
        solved = 0;
        while (solved == 0)
        {
            solved = tryguess(hashdic[o],dict[p]);
            p++;
        }
        o++; 
    }
    free(dict[0]);
    free(dict);
    free(hashd);
    free(hashdic);
}
